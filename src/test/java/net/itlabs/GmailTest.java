package net.itlabs;

import com.codeborne.selenide.Configuration;
import net.itlabs.pages.Gmail;
import net.itlabs.pages.Mails;
import net.itlabs.pages.Menu;
import org.junit.BeforeClass;
import org.junit.Test;
import testdata.Credential;

import static net.itlabs.Helpers.getUniqueText;


public class GmailTest {

    @BeforeClass
    public static void setUp() {
        Configuration.timeout = 10000;
    }



    @Test
    public void testLoginSendAndSearch() {
        Gmail.vizit();

        Gmail.logIn(Credential.email, Credential.password);

        String mailSubject = getUniqueText("test");
        Mails.send(Credential.email, mailSubject);

        Menu.refresh();

        Mails.assertMail(0, mailSubject);

        Menu.goToSent();
        Mails.assertMail(0, mailSubject);

        Menu.goToInbox();
        Mails.searchBySubject(mailSubject);

        Mails.assertMails(mailSubject);

    }
}
