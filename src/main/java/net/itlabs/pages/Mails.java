package net.itlabs.pages;

import com.codeborne.selenide.ElementsCollection;
import org.openqa.selenium.By;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;


/**
 * Created by ������ on 28.02.2016.
 */
public class Mails {
    public static ElementsCollection mails = $$("[role = main] .zA");


    public static void send(String email, String mailSubject) {

        $(byText("COMPOSE")).click();
        $(By.name("to")).setValue(email).click();
        $(By.name("subjectbox")).setValue(mailSubject).click();
        $(byText("Send")).click();
    }

    public static void assertMail(int index, String mailSubject) {
        mails.get(index).shouldHave(text(mailSubject));
    }


    public static void searchBySubject(String mailSubject) {
        $(By.name("q")).setValue("subject:"+mailSubject).pressEnter();
    }

    public static void assertMails(String... mailSubjects) {
        mails.shouldHave(texts(mailSubjects));
    }
}
