package net.itlabs.pages;

import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;

/**
 * Created by ������ on 28.02.2016.
 */
public class Menu {
    public static void refresh() {
        $(".asf").click();
    }

    public static void goToSent() {
        $(byText("Sent Mail")).click();
    }

    public static void goToInbox() {
        $(byText("Inbox")).click();
    }
}
